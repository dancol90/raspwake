#!/usr/bin/env python

import sys, logging, os, signal

from raspwake.service import startServices, stopServices, getService
from raspwake.core.flow import FlowControl
from raspwake.core.graphic import Surface
from raspwake.view import home, alarm

# Define a function that will handle exception and log to file
def exceptFunc(type, value, traceback):
    if issubclass(type, KeyboardInterrupt):
        sys.__excepthook__(type, value, traceback)
        return

    logging.error("Uncaught exception", exc_info=(type, value, traceback))

    cleanQuit();

def cleanQuit(*args):
    stopServices()


def alarmCallback(alarm):
	flow.new(alarm.AlarmView(alarm))

# Register for SIGINT signal so it can be killed gracefully
signal.signal(signal.SIGINT, cleanQuit)

# Change dir to the script one, so we can find all the resources using relative paths
os.chdir(os.path.dirname(os.path.abspath(__file__)))

# Init logging
#logging.basicConfig(filename="/tmp/raspwake.log", level = logging.INFO)
logging.basicConfig(level = logging.DEBUG)

sys.excepthook = exceptFunc


# Here begin initialization
logging.info("Initializing subsystems...")

# Create the main object
flow = FlowControl(home.Home)

# Load standard fonts
Surface.loadFont('text', 'Font/pf_tempesta_seven_compressed.ttf', 8)
Surface.loadFont('bold', 'Font/pf_tempesta_seven_compressed_bold.ttf', 8)
Surface.loadFont('tiny', 'Font/SDTHREE_0.ttf', 16) # it's actual 5x5 pixel

# Run all services. Blocking call.
startServices()

# Set callback for alarm
getService("AlarmService").setAlarmCallback(alarmCallback)

# Here we are, ready to go!
logging.info("Started")

# Let the flow begin
flow.start()

# If we arrive here all services have terminated their job. Stop everything and exit
logging.info("Gracefully exiting...")

stopServices()
sys.exit()


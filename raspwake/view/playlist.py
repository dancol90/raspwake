from os.path import basename, splitext

from raspwake.service import getService

from raspwake.core import constants
from raspwake.view import base, listview, icons

class Playlist(listview.ListView):
    def __init__(self):
        super(Playlist, self).__init__(None, None, 4)

        self.music = getService('MusicService')

        playingId = self.music.currentSongId()
        
        # Prepare the song list to be suitable for a List element
        for track in self.music.playlist():
            self.elements.append(( track['id'],
                              track['title'],
                              icons.play if track['id'] == playingId else icons.music,
                              track['file']
                            ))

        self.elements.append(('clear', '- Svuota playlist -', icons.remove))

    def update(self, key):
        if key == constants.SELECT:
            elementId = self.selected[0]
    
            if elementId == 'clear':
                self.music.stop()
                self.music.clear()
            else:
                self.music.playId(elementId)

            return base.BACK_TO_HOME
            
        return super(Playlist, self).update(key)

# -*- coding: utf-8 -*-

from raspwake.core import constants
from raspwake.core.base import Bitmap
from raspwake.view import base

class ListView(base.View):
    def __init__(self, title = 'Test', elements = (), rows = 4):
        super(ListView, self).__init__()
        
        self.title = title

        # If is set to None, create empty list. Probably subclasses will populate it.
        if elements is None:
            self.elements = []
        # If is not an empty list, copy it
        elif len(elements) > 0:
            self.elements = elements # use .copy()?
        # If is empty but not None, add a entry in the list
        else:
            self.elements = (('noelements', 'No elements', None),)

        self.selectedIndex = 0

        self.rows = rows

        if not self.title:
            listH = 64
        else:
            listH = 54

        self.rowHeight = listH // rows
        # For now fix the font height to 8px
        #self.rowPadding = (self.rowHeight + 8) // 2

        self.listPadding = (64 - listH) + (listH - self.rowHeight * rows) // 2

        self.window = slice(0, rows)

    def update(self, key):
        if key == constants.UP:
            self.prev()
            self.redraw = True
        elif key == constants.DOWN:
            self.next()
            self.redraw = True
        
        return base.DO_NOTHING

    def frame(self, surface):
        # Nota: quando si passa dall'indice 0 all'ultimo elemento della lista e viceversa,
        # non basta spostare la finestra di elementi visibili di una posizione in su o giù,
        # ci saranno più step da eseguire.
        # Essendo però che la funzione render viene chiamata ad ogni refresh dello schermo,
        # lascio che ad ogni frame ci si sposti di un elemento solo. In questo modo 
        # si crea un effetto animazione che dura tanti frame quanti sono gli elementi della lista.
        #if self.selectedIndex < self.window.start:
        #   self.window = slice(self.window.start - 1, self.window.stop - 1)
        #elif self.selectedIndex >= self.window.stop:
        #   self.window = slice(self.window.start + 1, self.window.stop + 1)
        surface.clear()

        if self.selectedIndex < self.window.start:
            self.window = slice(self.selectedIndex, self.selectedIndex + self.rows)
        elif self.selectedIndex >= self.window.stop:
            self.window = slice(self.selectedIndex - self.rows + 1, self.selectedIndex + 1)

        if self.title:
            surface.fillRect(0, 0, 128, 9)
            surface.centerText(self.title, y0 = 1, h = 8, color = constants.COLOR_OFF)

        y = self.listPadding

        for i, element in enumerate(self.elements[self.window]):
            if self.selectedIndex == i + self.window.start:
                color = constants.COLOR_OFF
                surface.fillRect(0, y, 128, self.rowHeight)
            else:
                color = constants.COLOR_ON

            if isinstance(element[2], Bitmap):
                surface.bitblt(element[2], 3, y + (self.rowHeight - element[2].height) // 2, color)
                x = 6 + element[2].width
            else:
                x = 4

            # Draw vertically centered text (w = 0 means no horizontal centering)
            surface.centerText(element[1], x0 = x, y0 = y, w = 0, h = self.rowHeight, color = color)
            
            # To speed things up we can assume that the text is 8px high and define rowPadding in __init__
            #surface.drawText(element[1], x, y + self.rowPadding, color = color)

            y += self.rowHeight

    def cycleIndex(self, i):
        count = len(self.elements)
        return (count + i) % count

    def next(self):
        self.selectedIndex = self.cycleIndex(self.selectedIndex + 1) 

    def prev(self):
        self.selectedIndex = self.cycleIndex(self.selectedIndex - 1) 

    @property
    def selected(self):
        return self.elements[self.selectedIndex]

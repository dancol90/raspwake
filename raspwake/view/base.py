from time import time

import logging

CLOSE_VIEW   = 1
BACK_TO_HOME = 2
DO_NOTHING   = 3
IGNORE_INPUT = 4

class View(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

        #self.logger.warning("Creating class")

        # Setting to 0 allows to tick for the first time immediately
        self._currentTime = 0

        self._timerInterval = 1

        self.redraw = True

        self._response = None
        self._id = None

        self._close_requested = False
        
    def activate(self, response = None, viewId = None):
        pass

    def frame(self, surface):
        raise Exception("NotImplementedException")

    def update(self, key):
        raise Exception("NotImplementedException")

    def close(self):
        self._close_requested = True
        self.redraw = False

    @property
    def isClosing(self):
        return self._close_requested


    def setTimer(self, interval):
        self._timerInterval = interval

    def tick(self):
        if time() - self._currentTime > self._timerInterval:
            self._currentTime = time()

            return True
        else:
            return False

    def forceTick(self):
        # Force tick() to return True
        self._currentTime = 0


    @property
    def viewId(self):
        return self._id
    @viewId.setter
    def viewId(self, value):
        self._id = value
    
    @property
    def response(self):
        return self._response
    @response.setter
    def response(self, value):
        self._response = value


    # Set the response and return value to close the view
    def setResponse(self, ret):
        self.response = ret

        return CLOSE_VIEW

    def hasReturned(self):
        return sefl._return is not None
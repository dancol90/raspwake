from raspwake.core.graphic import Surface
from raspwake.core import constants
from raspwake.view import base, listview, icons
from raspwake.service import getService

class NetworkManagerView(listview.ListView):
    def __init__(self):
        super(NetworkManagerView, self).__init__('Connessioni di rete', None, 3)

    def activate(self, response = None, viewId = None):
        self.elements.append(('wifi', 'HomeWiFi', icons.wifi[0], None))
        self.elements.append(('wifi', 'D-Link',   icons.wifi[1], None))
        self.elements.append(('wifi', 'Telecom-15621273', icons.wifi[2], None))
        self.elements.append(('wifi', 'Aria', icons.wifi[3], None))
        self.elements.append(('wifi', 'DragonsDen', icons.wifi[4], None))

    def update(self, key):
        elementAction = self.selected[0]
        elementData = self.selected[3]

        if key == constants.SELECT:
            if elementAction == 'wifi':
                pass
                #return AlarmUpdateMenu(elementData)

        return super(NetworkManagerView, self).update(key)


class PasswordInputView(base.View):
    def __init__(self):
        super(PasswordInputView, self).__init__()

        self.input = [0, ]
        self.selected = 0

    def activate(self, response = None, viewId = None):
        pass

    def update(self, key):
        self.redraw = True

        if key == constants.LEFT:
            self.deltaIndex(-1)
        elif key == constants.RIGHT:
            self.deltaIndex(+1)
        elif key == constants.UP:
            self.deltaChar(+1)
        elif key == constants.DOWN:
            self.deltaChar(-1)
        elif key == constants.SELECT:
            return self.setResponse(self.input)

        # If BACK is pressed and we have chars to delete, delete it.
        # If the input is empty, just one zero char remains, so the 
        # self.input list is never truly empty-
        elif key == constants.BACK and len(self.input) > 1:
            # If we have a char before the selected one to delete...
            if self.selected > 0:
                # ...delete it from the list...
                del self.input[self.selected - 1]
                # ...and move backward on selection to reflect the changes
                self.deltaIndex(-1)

            # Signal the system that BACK wasn't used to exit the view
            return base.IGNORE_INPUT
        else:
            self.redraw = False

        return base.DO_NOTHING

    def frame(self, surface):
        surface.clear()

        # All chars before the selected one are hidden
        before = '*' * self.selected
        # And so the ones after, except for the "special" last one thats alway a zero,
        # a placeholder for a new char that has not been modified
        after  = '*' * len(self.input[self.selected + 1:-1])

        # The char itself has to be shown, but if it the last one use a space
        # (ASCII char 0 is not printable)
        c = chr(self.input[self.selected]) if self.input[self.selected] != 0 else ' '

        # Draw the first part of the string and save the rendered width
        before_w  = surface.drawText(before, 10, 18)
        # Draw the selected char after the string drawn before
        char_w    = surface.drawText(c, 10 + before_w, 18)
        surface.drawText(after,  10 + before_w + char_w, 18)

        # Finally draw the rest of the string
        surface.drawHLine(10 + before_w, 18, char_w)


    def deltaIndex(self, delta):
        self.selected = (self.selected + delta) % len(self.input)


    def deltaChar(self, delta):
        if self.input[self.selected] == 0:
            self.input[self.selected] = 97
        else:
            self.input[self.selected] = 32 + (self.input[self.selected] - 32 + delta) % 95 

        if self.input[-1] >= 32:
            self.input.append(0)
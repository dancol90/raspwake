# -*- coding: utf-8 -*-

import re
import urllib2
import threading
from bs4 import BeautifulSoup

from datetime import date, timedelta

from raspwake.core import constants
from raspwake.core.graphic import Surface
from raspwake.view import base


class Weather(base.View):
    def __init__(self):
        super(Weather, self).__init__()

        Surface.loadFont('weather', 'Font/weathericons-regular.ttf', 22)
        Surface.loadFont('weather_small', 'Font/weathericons-regular.ttf', 14)

        self.oldStatus   = None
        self.status      = 'idle'

        self.weatherInfo = None

        self.thread = threading.Thread(target=self.getWeatherInfo)
        self.thread.start()

    def update(self, key):
        # Override standard back button to return straight to home
        if key == constants.BACK:
            return base.BACK_TO_HOME

        if self.status != self.oldStatus:
            self.oldStatus = self.status
            self.redraw = True

        return base.DO_NOTHING

    def frame(self, surface):
        surface.clear()

        if self.weatherInfo is None:
            if self.status == 'idle':
                message = 'Caricamento...'
            elif self.status == 'error':
                message = 'Non disponibile'

            surface.centerText(message, x0=0, y0=0, w = 128, h = 64, font="bold")
        else:
            x = 0

            # 40 + 4 + 40 + 4 + 40 = 128px width
            for day in self.weatherInfo:
                text = day['date'].strftime('%a %%s') % day['date'].day

                surface.centerText(text, x0 = x, y0 = 11, w = 40)

                text = Weather.weatherIcons[day['icon']]

                surface.centerText(text, x0 = x, y0 = 32, w = 40, font = 'weather')

                text = u"%d°C | %d°C" % (day['min_temp'], day['max_temp'])

                surface.centerText(text, x0 = x, y0 = 50, w = 40)

                text = u"%d%%" % (day['prob'] * 100)

                surface.drawText(u'\uf04e', x + 10, 62, 'weather_small')
                surface.drawText(text, x + 19, 62)

                x += 44

        self.redraw = False

    weatherIcons = {'w1' : u'\uf00d',
                    'w2' : u'\uf00d',
                    'w3' : u'\uf00c',
                    'w4' : u'\uf002',
                    'w5' : u'\uf00b',
                    'w6' : u'\uf006',
                    'w7' : u'\uf00a',
                    'w8' : u'\uf013',
                    'w9' : u'\uf01c',
                    'w10': u'\uf019',
                    'w11': u'\uf01b',
                    'w12': u'\uf017',
                    'w13': u'\uf01e',
                    'w14': u'\uf014',
                    'w15': u'\uf003',
                    'w16': u'\uf00e',
                    'w17': u'\uf015',
                    'w18': u'\uf01b',

                    # TODO: aggiungere icone notte w101-w118
                   }

    def getWeatherInfo(self):
        try:
            page = urllib2.urlopen('http://www.ilmeteo.it/box/previsioni.php?citta=4897&type=day1&days=3').read()
            parser = BeautifulSoup(page)

            rows = parser.select("div#box > table > tr")

            days = []

            dayDate   = date.today()
            oneDay    = timedelta(days = 1)

            for row in rows:
                cols = row.select('td.dati')

                # Probably the first row (legenda)
                if len(cols) == 0:
                    continue

                day = {}

                # Parse date
                day['date'] = dayDate #cols[0].string

                dayDate += oneDay

                # Parse weather icon
                icon = re.search(ur"ico1/([^\.]+)\.png", cols[1].find('img')['src'])

                if not icon is None:
                    day['icon'] = icon.group(1)
                else:
                    day['icon'] = 'none'


                # Parse temperatures
                day['min_temp']   = int(cols[2].string)
                day['max_temp']   = int(cols[3].string)

                # Parse wind direction
                day['wind_dir']   = cols[4].string

                # Parse window speed: "x km/h" or "assente"
                wind = cols[5].string

                if wind == 'assente':
                    day['wind_speed'] = 0
                else:
                    day['wind_speed'] = int(re.search(ur"(\d+)[^0-9]+", wind).group(1))

                # Parse probability: two td columns and both can contain percentage text
                pieces = cols[6].select('td')

                prob = ''

                for piece in pieces:
                    if not piece.string is None:
                        prob += piece.string

                day['prob'] = float(re.search(ur"\s*(\d+)%\s*", prob).group(1)) / 100


                # Day parsed
                days.append(day)

            self.logger.info("Done")

            self.status = 'done'
            self.weatherInfo = days
        except Exception:
            self.status = 'error'
            self.logger.error("Error loading weather info")

                
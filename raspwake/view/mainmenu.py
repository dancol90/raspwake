from raspwake.core import constants, flow
from raspwake.view import base, listview, musicdb, weather, playlist, alarm, network

from raspwake.view import icons

from raspwake.service import getService

class MainMenu(listview.ListView):
    def __init__(self):
        super(MainMenu, self).__init__(
            None,
            (('playlist', 'Coda di ascolto', icons.playlist_menu, playlist.Playlist),
             ('music',    'Musica', icons.music_menu, musicdb.MusicDB),
             ('weather',  'Previsioni Meteo', icons.weather_menu, weather.Weather),
             ('alarms',   'Sveglia', icons.alarms_menu, alarm.AlarmManagerView),
             ('settings', 'Impostazioni', icons.settings_menu, network.PasswordInputView)
            ),
            3)

    def activate(self, response = None, viewId = None):
        if response is not None:
            music = getService('MusicService')

            if viewId == 'music':
                music.clear()
                music.addSongs(response)

                self.close()

    def update(self, key):
        # If menu is pressed, do not create another menu view but return to Home
        if key == constants.MENU:
            return base.BACK_TO_HOME
        elif key == constants.SELECT:
            if not self.selected[3] is None:
                # Create the instance of the View to open
                view = self.selected[3]()
                # Set a mnemonic id. It will be used in activate()
                view.viewId = self.selected[0]

                return view
            
        return super(MainMenu, self).update(key)
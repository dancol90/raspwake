from raspwake.core.graphic import Surface
from raspwake.core import constants
from raspwake.view import base, listview, icons, musicdb
from raspwake.service import getService
from raspwake.service.alarm import Alarm

from datetime import datetime, timedelta

class AlarmView(base.View):
    def __init__(self, alarm):
        super(AlarmView, self).__init__()

        self.alarm = alarm

        self.music = getService("MusicService")

    def activate(self, response = None, viewId = None):
        self.music.clear()
        self.music.addSongs(self.alarm.tone)

    def update(self, key):
        if key == constants.SELECT:
            self.music.stop()

            return base.CLOSE_VIEW
            
        return base.IGNORE_INPUT

    def frame(self, surface):
        surface.clear()        
        surface.centerText(str(self.alarm), x0=0, y0=0, w = 128, h = 64, font="bold")




class AlarmManagerView(listview.ListView):
    def __init__(self):
        super(AlarmManagerView, self).__init__(None, None, 4)

        self.alarms = getService("AlarmService")

    def activate(self, response = None, viewId = None):
        # Non distinguo fra AlarmUpdateMenu e TimeInputView
        # perche' sono sicuro che solo la seconda mi ritorna qualcosa
        if response is not None:
            self.alarms.addAlarm(Alarm(response.hour, response.minute))

        self.elements = [('alarm', str(alarm), icons.alarm_on if alarm.enabled else icons.alarm_off, alarm) for alarm in self.alarms.alarms]
        self.elements.append(('add', 'Nuovo allarme', icons.plus, None))

    def update(self, key):
        elementAction = self.selected[0]
        elementData = self.selected[3]

        if key == constants.SELECT:
            if elementAction == 'alarm':
                return AlarmUpdateMenu(elementData)
            elif elementAction == 'add':            
                return TimeInputView("Nuova sveglia")

        return super(AlarmManagerView, self).update(key)



class AlarmUpdateMenu(listview.ListView):
    def __init__(self, alarm):
        super(AlarmUpdateMenu, self).__init__(
            str(alarm),
            (('enable', 'Disabilita' if alarm.enabled else 'Abilita',  None, not alarm.enabled),
             ('change', 'Modifica', None, None),
             ('tone',   'Suoneria', None, None),
             ('delete', 'Elimina',  None, None)
            ),
            3
        )

        self.alarm = alarm

    def activate(self, response = None, viewId = None):
        if response is not None:
            if viewId == 'alarm':
                self.alarm.setTime(response.hour, response.minute)
                self.title = str(self.alarm)
            elif viewId == 'tone':
                self.logger.debug("Selected song: %s", response)
                self.alarm.setTone(response)

    def update(self, key):
        elementAction = self.selected[0]
        elementData = self.selected[3]

        if key == constants.SELECT:
            
            if elementAction == 'change':
                view = TimeInputView("Imposta sveglia", self.alarm.getTime())
                view.viewId = 'alarm'
                return view

            elif elementAction == 'tone':
                view = musicdb.MusicDB()
                view.viewId = 'tone'
                return view

            elif elementAction == 'enable':
                self.alarm.enabled = elementData
                return base.CLOSE_VIEW

            elif elementAction == 'delete':
                getService("AlarmService").removeAlarm(self.alarm)
                return base.CLOSE_VIEW

        return super(AlarmUpdateMenu, self).update(key)


class TimeInputView(base.View):
    def __init__(self, title = None, time = None):
        super(TimeInputView, self).__init__()

        Surface.loadFont('alarm_bold', 'Font/Roboto-Bold.ttf', 35)
        Surface.loadFont('alarm', 'Font/Roboto-Bold.ttf', 21)

        self.title = title

        if isinstance(time, datetime):
            self.time = time
        else:
            self.time = datetime.now()

        self.selected = 0

        self.deltas = (timedelta(hours = 1), timedelta(minutes = 1))
        self.fonts  = (('alarm_bold', 'alarm'), ('alarm', 'alarm_bold'))

        self.confirmed = False

    def update(self, key):
        self.redraw = True

        if key == constants.LEFT:
            self.selected = (self.selected - 1) % 2
        elif key == constants.RIGHT:
            self.selected = (self.selected + 1) % 2
        elif key == constants.UP:
            self.time += self.deltas[self.selected]
        elif key == constants.DOWN:
            self.time -= self.deltas[self.selected]
        elif key == constants.SELECT:
            return self.setResponse(self.time)
        else:
            self.redraw = False

        return base.DO_NOTHING

    def frame(self, surface):
        surface.clear()

        if self.title:
            surface.fillRect(0, 0, 128, 9)
            surface.centerText(self.title, y0 = 1, color = constants.COLOR_OFF)

            y = 10
        else:
            y = 0

        strings = ("%02d" % self.time.hour, "%02d" % self.time.minute)
        fonts   = self.fonts[self.selected]

        widths = [Surface.fontCache[f].textDimensions(s)[0] for s, f in zip(strings, fonts)]

        x = (128 - sum(widths)) // 2

        for string, font, width in zip(strings, fonts, widths):
            surface.centerText(text = string, x0 = x, w = 0, y0 = y, h = 64-y, font = font)

            x += width
from os.path import basename, splitext

from raspwake.service import getService

from raspwake.core import constants
from raspwake.view import base, listview, icons

#from pprint import pprint

NEXT_TAG = {'artist': ('album', icons.album),
            'album':  ('songs', icons.music),
            'genre':  ('album', icons.album),
           }

class MusicDB(listview.ListView):
    # elements is a tuple or list of element:
    # Each element is a tuple of size 2, the first element is the keyword,
    # the second is the text to display
    def __init__(self, title = 'Music DB', elements = None):
        super(MusicDB, self).__init__(title, elements, 4)

        self.music = getService('MusicService')
        
        if elements is None:
            # Get top-level folders. Each one of them is a USB device or a mounted network share.
            for folder in self.music.ls():
                name = folder['directory']

                if name.startswith('USB'):
                    icon = icons.usb
                elif name.startswith('NET'):
                    icon = icons.net
                else:
                    icon = None

                # Add only if it has a valid name
                if(icon is not None):
                    #                     type    title icon  data
                    self.elements.append(('base', name, icon, {'base': name}))

    def activate(self, response = None, viewId = None):
        if response is not None:
            self.response = response
            self.close()

        # Not needed, as super method is empty, but is correct to do this, I think
        super(MusicDB, self).activate(response, viewId)

    # Update view's data before rendering
    def update(self, key):
        if (key == constants.SELECT):
            return self.updateList()
            
        return super(MusicDB, self).update(key)

    # Internal function, create new MusicDB view 
    # from user selection
    def updateList(self):
        # Get selected info
        elementType  = self.selected[0]
        elementName  = self.selected[1]
        elementData  = self.selected[3] #if not self.menu.selected[3] is None else elementName

        elements = []

        if elementType == 'none':
            # Go back here

            # No more to do, return
            return base.CLOSE_VIEW

        elif elementType == 'song':
            # Song selected, ready to return

            # Set the response to return and close the view
            return self.setResponse(elementData)

        elif elementType == 'base':
            # Selected device, go to "Artists, Albums, Folders" menu
            elements = (('artist', 'Artisti',  icons.artist, elementData),
                        ('album',  'Album',    icons.album,  elementData),
                        ('genre',  'Generi',   icons.music,  elementData),
                        ('songs',  'Canzoni',  icons.music,  elementData),
                        ('folder', 'Cartelle', icons.folder, elementData)
                       )

        elif elementType == 'songs':
            entries = self.music.find(elementData)

            for entry in entries:
                # "entry" is a dictionary containing all song's tags. 
                # Obviously they matches the tags in "elementData", but some others are added.
                # So, I extract only the url from "entry" and append it to "elementData"
                filters = elementData.copy()
                filters['file'] = entry['file']

                elements.append(('song',
                                 entry.get('title', basename(entry['file'])),
                                 icons.music,
                                 filters
                               ))
            
        elif elementType == 'folder':
            # TODO
            pass

        else:
            # All other tags
            entries = self.music.list(elementType, elementData)

            tag, icon = NEXT_TAG[elementType]

            for entry in entries:
                filters = elementData.copy()
                filters[elementType] = entry

                elements.append((tag, entry or '<Sconosciuto>', icon, filters))
            
        # Create menu object
 
        if not elements:
            elements = (('none', 'Nessun elemento', None, ''),)

        return MusicDB(elementName, elements)
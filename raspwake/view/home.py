from datetime import datetime
from os.path  import basename, isfile

from raspwake.service import getService

from raspwake.core import constants
from raspwake.core.graphic import Surface
from raspwake.view import base, icons, mainmenu

class Home(base.View):
    STATUS_ICONS = { 'play' : icons.play,
                     'pause': icons.pause,
                     'stop' : icons.stop,
                   }

    def __init__(self):
        super(Home, self).__init__()

        Surface.loadFont('clock_bold', 'Font/Roboto-Bold.ttf',  45)

        self.setTimer(1)

        self.music = getService('MusicService')

        self.status = self.music.status()

        self.volume = self.status['volume']
        self.showVolume = 0

    def update(self, key):
        changed = True

        if key == constants.MENU:
            return mainmenu.MainMenu()
        elif key == constants.UP:
            self.volume += 1 if self.volume < 100 else 0
            self.showVolume = 3
        elif key == constants.DOWN:
            self.volume -= 1 if self.volume > 0 else 0
            self.showVolume = 3
        elif self.status['mode'] == self.music.MODE_MUSIC:
            if key == constants.SELECT:
                self.music.playpause()
            elif key == constants.LEFT:
                self.music.cdprev()
            elif key == constants.RIGHT:
                self.music.next()
            else:
                changed = False
        else:
            changed = False

        # Force update
        if changed: self.forceTick()

        if self.tick():
            self.redraw = True
            self.status = self.music.status()

            if self.volume != self.status['volume']:
                self.music.volume(self.volume)

        return base.DO_NOTHING

    def frame(self, surface):
        surface.clear()

        # Draw clock
        time = datetime.now().time()

        clock = '%02d:%02d' % (time.hour, time.minute)

        if self.status['mode'] == self.music.MODE_MUSIC:
            # Draw current song info
            surface.drawText(self.status['artist'], 3, 46, maxW = 115)
            surface.drawText(self.status['song'],   3, 56, 'bold', maxW = 115)

            # Draw progress bar
            if not self.showVolume:
                self.drawProgress( surface,
                                   self.status['position'],
                                   self.status['duration'],
                                   formatSeconds(self.status['position']),
                                   formatSeconds(self.status['duration'])
                                 )

            surface.bitblt(self.STATUS_ICONS[self.status['state']], 120, 49)

            if self.status['shuffle'] :
                surface.bitblt(icons.shuffle, 120, 41)

            if self.status['single'] :
                surface.bitblt(icons.single, 120, 33)
            elif self.status['repeat'] :
                surface.bitblt(icons.repeat, 120, 33)

            # Clock rect height
            h = 38
        elif self.status['mode'] == self.music.MODE_AIRPLAY:
            surface.centerText("AirPlay", y0 = 46, h = 25, font = 'bold')
            h = 38
        elif self.status['mode'] == self.music.MODE_NO_MUSIC:
            h = 64

        surface.centerText(clock, x0 = 2, y0 = 0, w = 124, h = h, font = 'clock_bold')

        if self.showVolume:
            self.drawProgress(surface, self.volume, 100, '  Vol ', str(self.volume))
            self.showVolume -= 1

    def drawProgress(self, surface, val, total, textLeft, textRight = None):
        if total > 0:
            w = 81 * val // total
        else:
            w = 0

        surface.fillRect(    23, 59,      w, 5)
        surface.drawRect(w + 23, 59, 82 - w, 5)

        surface.drawText(textLeft,  0,   64, 'tiny')
        if textRight is not None:
            surface.drawText(textRight, 106, 64, 'tiny')

def formatSeconds(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    time = '%02d:%02d' % (m, s)

    if(h > 0): time = ('%02d:' % h) + time

    return time
import os

from raspwake.service.base import Service, registerService

class HardwareManager(Service):
    def serviceStart(self):
        self.devices = {}

        machine = os.uname()[4]

        # TODO: load hardware from configuration file

        # Load modules and classes based on system
        if machine.startswith('x86_64'):

            from raspwake.hw.onscreen  import OnScreenDisplay, KeyboardInput

            # Onscreen virtual display
            self.devices['display'] = OnScreenDisplay(3, 1)
            # Input from keyboard
            self.devices['input']   = KeyboardInput()

        elif machine.startswith('arm'):

            from raspwake.hw.raspberry import ST7920
            from raspwake.hw.companion import CompanionInput

            # Hardware display
            self.devices['display'] = ST7920()
            # Hardware key
            self.devices['input']   = CompanionInput()

        # Init loaded devices
        for device in self.devices.itervalues():
            device.init()

    def serviceStop(self):
        for device in self.devices.itervalues():
            device.end()

    def getDevice(self, name):
        return self.devices.get(name, None)

registerService(HardwareManager, 1)
import mpd, sys
from os.path import basename, isfile

from raspwake.service.base import Service, registerService

class MusicService(Service):
    MODE_NO_MUSIC = 0
    MODE_MUSIC    = 1
    MODE_AIRPLAY  = 2
    
    def serviceStart(self):
        self.client = mpd.MPDClient(use_unicode = True)

        self.client.iterate = True
        self.client.timeout = 3
        try:
            self.client.connect('localhost', 6600)

            self.client.consume(0)
            self.client.random(0)
            self.client.repeat(0)
            self.client.single(0)

            self._connected = True
        except Exception as e:
            self.logger.error("Error while connecting: %s" % str(e))

            self._connected = False

    def serviceStop(self):
        try:
            if self._connected:
                self.client.close()
        except Exception as e:
            pass

    def play(self, pos = None):
        if not pos is None:
            # Play song at position pos in the playlist
            self.client.play(pos)
        else:
            # Just unpause
            self.client.pause(0)
    def pause(self):
        self.client.pause(1)
        
    def playpause(self):
        self.client.pause()

    def stop(self):
        self.client.stop()

    def next(self):
        self.client.next()
    def prev(self):
        self.client.previous()
    def cdprev(self):
        # Get raw status data from client
        status = self.client.status()
        elapsed = int(status.get('time', '0:0').split(':')[0])

        if elapsed < 3:
            self.prev()
        else:
            self.client.seekcur(0)

    def clear(self):
        self.client.clear()

    def add(self, uri):
        return self.client.addid(uri)

    def volume(self, vol):
        if 0 <= vol <= 100:
            self.client.setvol(vol)

    # Add all songs that matches "filters"
    # If "filters" contains a 'file' key
    # the corresponding song will be the first to be played
    def addSongs(self, filters):
        # Remove 'file' tag from filters
        strippedFilters = filters.copy()
        strippedFilters.pop('file', None)

        # Add all songs that match the filters
        songs = self.findadd(strippedFilters)

        # If a 'file' tag was specified...
        if 'file' in filters:
            # ...find the last time it appears in the playlist...
            startFromSong = list(self.client.playlistfind('filename', filters['file']))[-1]

            # ...and use its id to start playing from that song
            self.client.playid(startFromSong['id'])

    def isPlaying(self):
        return self.client.status()['state'] == 'play'

    def status(self):
        stat = {}

        if isfile("/tmp/shairplaying"):
            stat['mode'] = self.MODE_AIRPLAY
        else:
            songInfo = self.client.currentsong()
            status   = self.client.status()

            stat['volume'] = int(status['volume'])

            if songInfo:
                stat['artist'] = songInfo.get('artist', 'Unknown artist')
                stat['song']   = songInfo.get('title', basename(songInfo.get('file', '')))

                position = status.get('time', '0:0').split(':')

                stat['position'] = int(position[0])
                stat['duration'] = int(position[1])

                stat['playlist_pos'] = int(status.get('song', 0))

                # Playing, pause etc
                stat['state']   = status['state']

                # Playback mode
                stat['shuffle'] = status['random'] == '1'
                stat['single']  = status['single'] == '1'
                stat['repeat']  = status['repeat'] == '1'

                # Which source are we listening to?
                stat['mode'] = self.MODE_MUSIC
            else:
                stat['mode'] = self.MODE_NO_MUSIC

        return stat

    def playId(self, playlistId):
        self.client.playid(playlistId)

    def currentSongId(self):
        return int(self.client.currentsong().get('id', -1))

    # Generator for the current playlist
    def playlist(self):
        for track in self.client.playlistinfo():
            yield { 'id': int(track['id']),
                    'title': track.get('title', basename(track['file'])),
                    'file' : track['file']
                  }


    def list(self, tag, filters = {}):
        args = (i for pair in filters.iteritems() for i in pair)

        results = self.client.list(tag, *args)

        # Sort alphabetically
        return sorted(results, key=lambda y: y.lower())

    def find(self, filters = {}):
        args = (i for pair in filters.iteritems() for i in pair)

        # Call find with args elements as arguments
        return self.client.find(*args)

    def findadd(self, filters = {}):
        args = (i for pair in filters.iteritems() for i in pair)

        self.client.findadd(*args)

    def ls(self, uri = ''):
        return self.client.lsinfo(uri)

registerService(MusicService, 2)
import logging 

class Service(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def serviceStart(self):
        raise Exception("NotImplementedException")

    def serviceStop(self):
        raise Exception("NotImplementedException")


services = {}

def registerService(serviceClass, priority):
	services[serviceClass.__name__] = (serviceClass(), priority)

def startServices():
	sortedServices = sorted(services.itervalues(), key=lambda a: a[1])

	for service in sortedServices:
		service[0].serviceStart()
		#print "Starting %s" % service[0].__class__.__name__

def stopServices():
	sortedServices = sorted(services.itervalues(), key=lambda a: a[1])

	for service in sortedServices:
		service[0].serviceStop()
		#print "Ending %s" % service[0].__class__.__name__

def getService(name):
	return services[name][0]
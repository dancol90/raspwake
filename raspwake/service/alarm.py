import time, datetime
from threading import Thread

from raspwake.service.base import Service, registerService, getService

class Alarm(object):
    def __init__(self, hour, minute, days = None):
        self.setTime(hour, minute)
        self.days = days

        self.triggered = False

        self.enabled = True

    def check(self):
        now = time.localtime()

        if self.hour == now.tm_hour and self.minute == now.tm_min:
            if not self.triggered:
                self.triggered = True
                return True
        else:
            self.triggered = False
        
        return False

    def setTime(self, hour, minute):
        self.hour = hour
        self.minute = minute

    def getTime(self):
        return datetime.datetime.now().replace(hour = self.hour, minute = self.minute)

    def setTone(self, url):
        self.tone = url

    def __str__(self):
        return "%02d:%02d" % (self.hour, self.minute)
            

class AlarmService(Service, Thread):
    def __init__(self):
        Thread.__init__(self)

    def serviceStart(self):
        #self.lock = Lock()
        self.running = True

        self.alarmCallback = None

        self.alarms = [Alarm(11,22), Alarm(12,23), Alarm(13,24)]

        self.start()

    def serviceStop(self):
        self.running = False

        self.join()

    def setAlarmCallback(self, callback):
        self.alarmCallback = callback

    def addAlarm(self, alarm):
        if isinstance(alarm, Alarm):
            self.alarms.append(alarm)

    def removeAlarm(self, alarm):
        try:
            if isinstance(alarm, Alarm):
                self.alarms.remove(alarm)       
        except:
            print "error"

    def run(self):
        while self.running:
            # Check only if there's a use for the info
            if self.alarmCallback:
                for alarm in self.alarms:
                    if alarm.check():
                        self.alarmCallback(alarm)

            time.sleep(1)


registerService(AlarmService, 4)
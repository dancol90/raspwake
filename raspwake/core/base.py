from raspwake.core.constants import COLOR_ON, COLOR_OFF

class Bitmap(object):
    def __init__(self, width, height, pixels = None):
        if pixels is not None:
            if len(pixels) != width * height:
                raise Exception("WrongDimensions")

            self.pixels = pixels
        else:
            self.pixels = bytearray(width * height)

        self.width  = width
        self.height = height

    def clear(self):
        del self.pixels
        self.pixels = bytearray(self.width * self.height)

    def bitblt(self, src, x, y, color = COLOR_ON, blend = False):
        """Copy all pixels from `src` into this bitmap"""
        
        if (x < 0): w0 = abs(x)
        else:       w0 = 0

        if (y < 0): h0 = abs(y)
        else:       h0 = 0

        w1 = min(src.width, self.width - x)
        h1 = min(src.height, self.height - y)

        for sy in range(h0, h1):
            for sx in range(w0, w1):
                pos = (y + sy) * self.width + x + sx
                
                pixel = src.pixels[sy * src.width + sx]

                if pixel or not blend:
                    self.pixels[pos] = (pixel == color)
from raspwake.core import constants
from raspwake.core.base import Bitmap
from raspwake.core.font import Font

class Surface(Bitmap):
    fontCache = {}

    def __init__(self, width, height, pixels = None):
        super(Surface, self).__init__(width, height, pixels)

    def setPixel(self, x, y, color = constants.COLOR_ON):
        if 0 <= x < self.width and 0 <= y < self.height:
            self.pixels[y * self.width + x] = color

    def drawHLine(self, x, y, w, color = constants.COLOR_ON):
        for i in range(x, x + w):
            self.setPixel(i, y, color)      

    def drawVLine(self, x, y, h, color = constants.COLOR_ON):
        for i in range(y, y + h):
            self.setPixel(x, i, color)

    def drawLine(self, x0, y0, x1, y1, color = constants.COLOR_ON):
        steep = abs(y1 - y0) > abs(x1 - x0)

        if steep:
            x0, y0 = y0, x0  
            x1, y1 = y1, x1

        if x0 > x1:
            x0, x1 = x1, x0
            y0, y1 = y1, y0
        
        deltax = x1 - x0
        deltay = abs(y1 - y0)
        error = 0
        y = y0
            
        for x in range(x0, x1):
            if steep:
                self.setPixel(y, x, color)
            else:
                self.setPixel(x, y, color)
            
            if (error << 1) >= deltax:
                y = y + 1
                error = error + 2*(deltay - deltax)
            else:
                error = error + 2*deltay

    def drawRect(self, x, y, w, h, color = constants.COLOR_ON):
        self.drawHLine(x, y, w, color)
        self.drawVLine(x, y, h, color)
        self.drawHLine(x, y + h - 1, w, color)
        self.drawVLine(x + w - 1, y, h, color)

    def fillRect(self, x, y, w, h, color = constants.COLOR_ON):
        for i in range(x, x + w):
            for j in range(y, y + h):
                self.setPixel(i, j, color)

    # NOTE: y is the coordinate for the baseline
    def drawText(self, text, x, y, font = 'text', color = constants.COLOR_ON, maxW = 0, maxH = 0):
        if(not isinstance(font, Font)):
            font = Surface.fontCache[font]

        width = font.renderText(text, self, x, y, color, w = maxW)

        return width

    # NOTE: y indicate the baseline if h != 0, otherwise the top left corner of the bounding rect
    def centerText(self, text, y0, font = 'text', color = constants.COLOR_ON, x0 = 0, w = 128, h = 0):
        if(not isinstance(font, Font)):
            font = Surface.fontCache[font]
            
        width, height, _ = font.textDimensions(text)

        x = (w - width)  // 2 if w > 0 else 0
        y = (h + height) // 2 if h > 0 else 0

        self.drawText(text, x0 + x, y0 + y, font, color)

    @staticmethod
    def loadFont(key, fontName, fontSize):
        Surface.fontCache[key] = Font(fontName, fontSize)

# class ImageFile(Bitmap):
#   def __init__(self, filename):
#       image = PIL.Image.open("convert_image.png")

#       # Convert image to black and white
#       image = image.convert('1')

#       super(Surface, self).__init__(image.size[0], image.size[1], image.load())
# Based on work by Daniel Bader
# http://dbader.org/blog/monochrome-font-rendering-with-freetype-and-python

import freetype

from raspwake.core.base import Bitmap
from raspwake.core import constants

class Glyph(Bitmap):
    def __init__(self, glyph):
        super(Glyph, self).__init__(glyph.bitmap.width,
                                     glyph.bitmap.rows,
                                     Glyph.unpackMonoBitmap(glyph.bitmap))

        # The glyph bitmap's top-side bearing, i.e. the vertical distance from the
        # baseline to the bitmap's top-most scanline.
        self.top = glyph.bitmap_top

        # Ascent and descent determine how many pixels the glyph extends
        # above or below the baseline.
        self.descent = max(0, self.height - self.top)
        #self.ascent = max(0, max(self.top, self.height) - self.descent)
        self.ascent = self.top

        # The advance width determines where to place the next character horizontally,
        # that is, how many pixels we move to the right to draw the next glyph.
        # The advance width is given in FreeType's 26.6 fixed point format,
        # which means that the pixel values are multiples of 64.
        self.advance_width = glyph.advance.x / 64

    @staticmethod
    def unpackMonoBitmap(bitmap):
        """
        Unpack a freetype FT_LOAD_TARGET_MONO glyph bitmap into a bytearray where each
        pixel is represented by a single byte.
        """
        # Allocate a bytearray of sufficient size to hold the glyph bitmap.
        data = bytearray(bitmap.rows * bitmap.width)

        # Iterate over every byte in the glyph bitmap. Note that we're not
        # iterating over every pixel in the resulting unpacked bitmap --
        # we're iterating over the packed bytes in the input bitmap.
        for y in range(bitmap.rows):
            for byte_index in range(bitmap.pitch):

                # Read the byte that contains the packed pixel data.
                byte_value = bitmap.buffer[y * bitmap.pitch + byte_index]

                # We've processed this many bits (=pixels) so far. This determines
                # where we'll read the next batch of pixels from.
                num_bits_done = byte_index * 8

                # Pre-compute where to write the pixels that we're going
                # to unpack from the current byte in the glyph bitmap.
                rowstart = y * bitmap.width + byte_index * 8

                # Iterate over every bit (=pixel) that's still a part of the
                # output bitmap. Sometimes we're only unpacking a fraction of a byte
                # because glyphs may not always fit on a byte boundary. So we make sure
                # to stop if we unpack past the current row of pixels.
                for bit_index in range(min(8, bitmap.width - num_bits_done)):

                    # Unpack the next pixel from the current glyph byte.
                    bit = byte_value & (1 << (7 - bit_index))

                    # Write the pixel to the output bytearray. We ensure that `off`
                    # pixels have a value of 0 and `on` pixels have a value of 1.
                    data[rowstart + bit_index] = 1 if bit else 0

        return data


class Font(object):
    def __init__(self, filename, size):
        self.face = freetype.Face(filename)
        self.face.set_pixel_sizes(0, size)

        self.glyphCache = {}
        self.textCache = {}

    def getGlyph(self, char):
        if not char in self.glyphCache:
            # Let FreeType load the glyph for the given character and tell it to render
            # a monochromatic bitmap representation.
            self.face.load_char(char, freetype.FT_LOAD_RENDER | freetype.FT_LOAD_TARGET_MONO)

            self.glyphCache[char] = Glyph(self.face.glyph)

        return self.glyphCache[char]

    def kerningOffset(self, previous_char, char):
        """
        Return the horizontal kerning offset in pixels when rendering `char`
        after `previous_char`.

        Use the resulting offset to adjust the glyph's drawing position to
        reduces extra diagonal whitespace, for example in the string "AV" the
        bitmaps for "A" and "V" may overlap slightly with some fonts. In this
        case the glyph for "V" has a negative horizontal kerning offset as it is
        moved slightly towards the "A".
        """
        kerning = self.face.get_kerning(previous_char, char)

        # The kerning offset is given in FreeType's 26.6 fixed point format,
        # which means that the pixel values are multiples of 64.
        return kerning.x / 64

    def textDimensions(self, text):
        """Return (width, height, baseline) of `text` rendered in the current font."""

        if not text in self.textCache:
            width = 0
            max_ascent = 0
            max_descent = 0
            previous_char = None

            # For each character in the text string we get the glyph
            # and update the overall dimensions of the resulting bitmap.
            for char in text:
                glyph = self.getGlyph(char)
                max_ascent = max(max_ascent, glyph.ascent)
                max_descent = max(max_descent, glyph.descent)
                kerning_x = self.kerningOffset(previous_char, char)

                # With kerning, the advance width may be less than the width of the glyph's bitmap.
                # Make sure we compute the total width so that all of the glyph's pixels
                # fit into the returned dimensions.
                width += max(glyph.advance_width + kerning_x, glyph.width + kerning_x)

                previous_char = char

            height = max_ascent + max_descent

            self.textCache[text] = (width, height, max_descent)

        return self.textCache[text]

    def renderText(self, text, dest, x0 = 0, y0 = 0, color = constants.COLOR_ON, w = 0, h = 0):
        x = x0
        previous_char = None

        for char in text:
            glyph = self.getGlyph(char)

            # Take kerning information into account before we render the
            # glyph to the output bitmap.
            x += self.kerningOffset(previous_char, char)

            if x - x0 + glyph.width > w and w != 0:
                break

            # The vertical drawing position should place the glyph
            # on the baseline as intended.
            y = y0 - glyph.top

            dest.bitblt(glyph, x, y, color, True)

            x += glyph.advance_width
            previous_char = char

        return x - x0

if __name__ == '__main__':
    # Be sure to place 'helvetica.ttf' (or any other ttf / otf font file) in the working directory.
    fnt = Font('play.ttf', 24)

    # Choosing the baseline correctly
    #print(repr(fnt.renderText('hello,'))

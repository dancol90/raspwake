import logging
import time

from raspwake.service      import getService

from raspwake.core         import constants
from raspwake.core.graphic import Surface
from raspwake.view         import base

class FlowControl(object):
    def __init__(self, homeView):
        self.homeView = homeView

    def start(self):
        self.logger = logging.getLogger(__name__)

        # Get a shorter name for hardware service
        self.input   = getService('HardwareManager').getDevice('input')
        self.display = getService('HardwareManager').getDevice('display')

        # View stack
        self.viewHistory = []

        self.surface = Surface(128, 64)

        # Load the home panel
        try:
            self.new(self.homeView())
        except AttributeError:
            self.logger.error("No Home View set.")

            return

        # Flag the main loop as running
        self.running = True
        
        # Actually start the main loop
        self.run()

    @property
    def current(self):
        # Return the first element of the stack (python stacks using lists push elements to the end)
        return self.viewHistory[-1]

    def new(self, view):
        self.logger.debug("New view %s", view)

        # Push the new view to the stack
        self.viewHistory.append(view)

        self.current.activate()

    def back(self):
        # If the stack has more than one view (which is the home view)...
        if len(self.viewHistory) > 1:
            self.logger.debug("Going back")

            # ...pop the current view and remove it
            view = self.viewHistory.pop()

            # Force redrawing, but activate() can cancel it 
            # by setting redraw to False
            self.current.redraw = True

            self.current.activate(view.response, view.viewId)

    def backAll(self):
        self.logger.debug("Going back to first view")
        
        # Discard all the view from stack except for the first inserted, which is the home view
        self.viewHistory = self.viewHistory[:1]


    def run(self):
        while self.running:
            # Get input from hardware
            key = self.input.get()

            if(self.current.isClosing):
                self.back()

            # Update the current view
            result = self.current.update(key)

            # result can be 
            #  - a View object: add this view
            #  - BACK_TO_HOME: go to first view in stack (usually Home)
            #  - DO_NOTHING: do nothing, stay here. User can exit from view by hitting back button

            if isinstance(result, base.View):
                # New view
                self.new(result)
            elif result == base.CLOSE_VIEW:
                self.back()
            elif result == base.BACK_TO_HOME:
                # Return to home
                self.backAll()
            elif result != base.IGNORE_INPUT and key > 0:
                # No overrides, standard input managment

                logging.debug("Key Input")

                #if key == constants.MENU:
                #    self.new(MainMenu())
                #elif key == constants.EXIT:
                if key == constants.EXIT:
                    self.running = False
                elif key == constants.BACK:
                    self.back()

            if self.current.redraw:
                # Let current view render
                self.current.frame(self.surface)

                # Display the current view
                self.display.draw(self.surface)

                self.current.redraw = False

            time.sleep(0.1)

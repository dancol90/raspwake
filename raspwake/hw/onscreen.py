import pygame
from pygame.locals import *

from raspwake.hw.base import Input, Display
from raspwake.core import constants

class KeyboardInput(Input):
    def init(self):
        # If no window is created than create a small one to receive events
        if pygame.display.get_surface() == None:
            pygame.init()
            self.surface = pygame.display.set_mode((10,10))
        
        pygame.event.set_allowed(None)
        pygame.event.set_allowed([pygame.QUIT, pygame.KEYDOWN])

    def end(self):
        pass

    def get(self):
        key = 0

        for event in pygame.event.get():
            if (event.type == pygame.KEYDOWN):
                if (event.key == pygame.K_LEFT):
                    key = constants.LEFT
                elif (event.key == pygame.K_RIGHT):
                    key = constants.RIGHT
                elif (event.key == pygame.K_UP):
                    key = constants.UP
                elif (event.key == pygame.K_DOWN):
                    key = constants.DOWN
                elif (event.key == pygame.K_q):
                    key = constants.EXIT
                elif (event.key == pygame.K_SPACE):
                    key = constants.SELECT
                elif (event.key == pygame.K_RETURN):
                    key = constants.MENU
                elif (event.key == pygame.K_BACKSPACE):
                    key = constants.BACK

        return key


class OnScreenDisplay(Display):
    def __init__(self, pixelDim = 4, margin = 1):
        self.off = ( 50, 100, 200)
        self.on  = (255, 255, 255)
        self.bg  = ( 30,  30, 255)

        self.pixelDim = pixelDim
        self.squareDim = pixelDim + margin

        pygame.init()


    def init(self):
        self.clock = pygame.time.Clock()

        # each dot is 4x4 pixels with 1px margin:
        #   w = 128 * (4px + 1px) = 640px
        #   h =  64 * (4px + 1px) = 320px
        self.surface = pygame.display.set_mode((128 * self.squareDim, 64 * self.squareDim))

        pygame.display.set_caption('ST7920')

        # init pixel grid
        self.surface.fill(self.bg)

    def end(self):
        pass

    def draw(self, surface):
        position = 0

        colors = (self.off, self.on)

        for y in range(surface.height):
            for x in range(surface.width):
                self.surface.fill(colors[surface.pixels[position]],
                                  [x * self.squareDim, 
                                   y * self.squareDim,
                                   self.pixelDim,
                                   self.pixelDim]
                                  )
                position += 1

        pygame.display.update()

        pygame.display.set_caption("%f FPS" % self.clock.get_fps())

        self.clock.tick(20)

    def clear(self):
        self.surface.fill(self.bg)

import smbus

from raspwake.hw.base import Input, Display
from raspwake.core import constants

class CompanionInput(Input):
    keyMapping = ( constants.MENU,   # Key 0 => menu
                   constants.BACK,   # Key 1 => back
                   constants.SELECT, # Key 2 => select (center key)
                   constants.UP,     # Key 3 => select (center key)
                   constants.DOWN,   # Key 4 => select (center key)
                   constants.LEFT,   # Key 5 => select (center key)
                   constants.RIGHT,  # Key 6 => select (center key)
                   constants.EXIT,   # TESTING ONLY
                 )

    def init(self):
        self.bus = smbus.SMBus(1)

        # Get button count with command 0x1f
        self.buttonCount = self.readCommand(0x1f)

        self.count = min(self.buttonCount, len(CompanionInput.keyMapping))

        # Temporary: set lcd backlight
        self.bus.write_byte_data(0x2d, 0x30, 200)

    def end(self):
        # Nothing to do
        pass

    def get(self):
        button = self.readCommand(0x10)

        if(0 < button <= self.buttonCount):
          return CompanionInput.keyMapping[button - 1]

        
    def readCommand(self, command):
        try:
            self.bus.write_byte(0x2d, command)

            return self.bus.read_byte(0x2d)
        except IOError:
            self.logger.error("Error requesting I2C data")  
            return 0

# -*- coding: utf-8 -*-

import spi
import RPi.GPIO as GPIO
import time

from raspwake.hw.base import Input, Display
from raspwake.core import constants

class GPIOInput(Input):
    buttons = { 3: constants.MENU,   # Key 0 => menu
                5: constants.BACK,   # Key 1 => back
                #3: constants.SELECT, # Key 2 => select (center key)
                #4: constants.UP,     # Key 3 => select (center key)
                #5: constants.DOWN,   # Key 4 => select (center key)
                #6: constants.LEFT,   # Key 5 => select (center key)
                #7: constants.RIGHT,  # Key 6 => select (center key)
              }

    rebounceTime = 200

    def init(self):
        # Button presses queue
        self.keys = []
        # Rebounce timing
        self.rebounce = {}

        GPIO.setmode(GPIO.BOARD)

        # For each configured pin...
        for pin in GPIOInput.buttons.keys():
            GPIO.setup(pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)

            # ...set a callback for both rising and fall edges, and set the pullup resistor
            GPIO.add_event_detect(pin, GPIO.BOTH,  callback = self.gpioChange, bouncetime = 50)

            # Set the rebounce time to 0 (which means not pressed)
            self.rebounce[pin] = 0

    def end(self):
        for pin in GPIOInput.buttons.keys():
            GPIO.remove_event_detect(pin)


    def get(self):
        # For each pin, check if it has been pressed long enough to retrigger it
        for pin, value in GPIOInput.buttons.iteritems():
            # Get last tigger and actual time
            rebounce = self.rebounce[pin]
            now = time.clock()

            # If the button in being held down, check if it's time to retrigger
            if rebounce > 0 and now - rebounce > GPIOInput.rebounceTime:
                # Add to the queue
                self.keys.append(value)
                # Reset timing to start over
                self.rebounce[pin] = now

        # Return the first key in the queue
        return self.keys.pop() if self.keys else 0

    def gpioChange(self, channel):
        # The button bring the level to ground
        if GPIO.input(channel) == GPIO.LOW:
            self.keys.append(GPIOInput.buttons[channel])
            self.rebounce[channel] = time.clock()
        else:
            self.rebounce[channel] = 0

# Raplace spi with SPIlib

class ST7920(Display):
    def __init__(self):
        # The openSPI() function is where the SPI interface is configured. There are
        #   three possible configuration options, and they all expect integer values:
        #   speed - the clock speed in Hz
        #   mode - the SPI mode (0, 1, 2, 3)
        #   bits - the length of each word, in bits (defaults to 8, which is standard)
        # It is also possible to pass a device name, spidevX.Y,
        spi.openSPI(speed = 2000000, mode = 3)

        self.framebuffer = bytearray(16 * 64)

        self.oldY = -1
        self.oldX = -1

    def init(self):
        # init the screen controller
        self.command(( 0x20, # Normal instruction set
                       0x0C, # Display on, cursor and char blink off
                       0x06, # Cursor move to right, increment ram address, no shift
                       0x02, # disable scroll, enable CGRAM adress
                       0x24, # Extended commands
                       0x26, # Enable graphic mode
                       0x01, # Clear display
                    ))
        
        # Clear the screen completely
        self.clear()
        self.draw()

    def end(self):
        self.clear()
        self.draw()
        spi.closeSPI()

    def draw(self, surface = None):
        blockNumber = 0

        if surface is None:
            for y in range(64):
                for x in range(8):
                    self.sendPage(x, y, (self.framebuffer[y*16 + 2*x], self.framebuffer[y*16 + 2*x + 1]))            
            return 
            
        for page in block(surface.pixels, 16):
            firstHalf  = bitsToByte(page[0:8])
            secondHalf = bitsToByte(page[8:16])

            if firstHalf  != self.framebuffer[blockNumber] or \
               secondHalf != self.framebuffer[blockNumber + 1]:

                # Calcolo le coordinate:
                # sto contanto 16 byte per riga, ma un blocco è composto da 2byte
                # avendo 8 blocchi da 16bit per riga da 128px.
                y, x = divmod(blockNumber / 2, 8)

                # Changed chunk, send to display
                self.sendPage(x, y, (firstHalf, secondHalf))

                # and save to framebuffer
                self.framebuffer[blockNumber]     = firstHalf
                self.framebuffer[blockNumber + 1] = secondHalf

            blockNumber += 2

    # This clear only the framebuffer.
    def clear(self):
        del self.framebuffer
        self.framebuffer = bytearray(16 * 64)

    # x, y in page coordinates:
    #  x in [0, 7] 
    #  y in [0, 63]
    # data è una lista con lunghezza multipla di due contenente byte
    # Ogni pagina infatti è composta da 16bit che corrispondono a 16px, per cui 2byte
    def sendPage(self, x, y, data):
        #print("Write page at (%s, %s): %s" % (x, y, " ".join("{0:08b}".format(b) for b in data)))

        # If the page is not adjacent to the last one,
        # set x and y address
        if y != self.oldY or x != self.oldX + 1:
            #print("Set address")

            self.command(( 0x80 | (y % 32) , 0x80 | (x + 8 * (y // 32)),))

        # send page data
        self.data(data)

        self.oldX = x
        self.oldY = y

    def command(self, data):
        #print("Write command: %s" % " ".join("{0:08b}".format(b) for b in data))

        seq = []

        for b in data:
            # 0xf8 = 0b11111000 RS=0, command
            seq.extend((0xf8, b & 0xf0, (b << 4) & 0xf0))
        
        spi.transfer(tuple(seq))

    def data(self, data):
        #print("Write data: %s" % " ".join("{0:08b}".format(b) for b in data))

        seq = []

        for b in data:
            # 0xfa = 0b111110a0 RS=a, data
            seq.extend((0xfa, b & 0xf0, (b << 4) & 0xf0))
        
        spi.transfer(tuple(seq))



def bitsToByte(bits):
    byte = 0

    for bit in bits:
        byte = (byte << 1) + (1 if bit else 0)

    return byte

def block(data, blockSize):
    for i in range(0, len(data), blockSize):
        yield data[i:i+blockSize]
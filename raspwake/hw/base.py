import logging

class Hardware(object):
    def init(self):
        raise Exception("NotImplementedException")

    def end(self):
        raise Exception("NotImplementedException")        


class Display(Hardware):
    def __init__(self):
        pass

    def draw(self, surface):
        raise Exception("NotImplementedException")

    def clear(self):
        raise Exception("NotImplementedException")


class Input(Hardware):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def get():
        raise Exception("NotImplementedException")
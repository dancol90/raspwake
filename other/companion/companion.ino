#include <avr/wdt.h>

#include <Keypad.h>
#include <Wire.h>

/*
  Fornisco lo stato dei vari pulsanti e 
  controllo la retroilluminazione dello schermo
  
  Magari controllo anche un eventuale led rgb per i giochi di luce
  
  Comandi in lettura:
    - (0x1F) Button count
    - (0x10 - 0x1E) Button_i: stato del pulsante i, con i da 0 a 9
  
  Comandi in scrittura:
    - (0x20 - 0x2F) Rebounce:  tempo per il rebounced del pulsante
    - (0x30) Backlight: intensità dell'illuminazione dello schermo. Valori da 0 a 255
    - (0xF0) Reset della scheda
  
*/
#define COMMAND_BUTTON_COUNT 0x1F
#define COMMAND_GET_BUTTONS  0x10
#define COMMAND_BACKLIGHT    0x30
#define COMMAND_RESET        0xF0
#define COMMAND_REBOUNCE     0x20

#define BUTTON_COUNT      8

#define PIN_BACKLIGHT     5
 
byte ret_value;
bool request_reset;

#define BTN_M 1
#define BTN_B 2
#define BTN_S 3
#define BTN_U 4
#define BTN_D 5
#define BTN_L 6
#define BTN_R 7
#define BTN_Q 8
#define NONE  0

byte btn_state;

//define the cymbols on the buttons of the keypads
// Everything is rotated 90 degree clockwise
char keymap[3][3] = {
  {BTN_M, BTN_U, BTN_Q},
  {BTN_L, BTN_S, BTN_R},
  {BTN_B, BTN_D, NONE },
};
byte rows[] = {7, 6, 5}; //connect to the row pinouts of the keypad
byte cols[] = {2, 3, 4}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keymap), rows, cols, 3, 3); 

void setup() {
  wdt_disable();
  
  Wire.begin(0x2D);             // join i2c bus with address #2
  
  Wire.onRequest(requestEvent); // register event
  Wire.onReceive(receiveEvent);
  
  // Set backlight pin
  //pinMode(PIN_BACKLIGHT, OUTPUT);
  
  // And turn it on at hlaf-bightness
  //analogWrite(PIN_BACKLIGHT, 128);
  
  btn_state = 0;
     
  request_reset = false;
}

void loop() {
  // Check if a reset has been requested
  if(request_reset) {
    // Disable interrupts to avoid receiving i2c data
    noInterrupts();
    // enable the watchdog timer to reset in 15ms
    wdt_enable(WDTO_15MS);
    // and wait
    while(1) {}
  }
  
  char k = keypad.getKey();
  
  if(k > 0 && k <= BUTTON_COUNT)
    //bitSet(btn_state, k-1);
    btn_state = k;
}

// function that executes whenever data is requested by master
void requestEvent() {
  // Just return the last computed value
  Wire.write(ret_value);
}

// function that executes whenever data is received from master
void receiveEvent(int howMany) {
  byte command, parameter;
  
  command = Wire.read();
  
  if(howMany > 1)
    parameter = Wire.read();
  else
    parameter = 0;
  
  // Se howMany > 2 non importa, sono stati mandati dati in più che non mi interessano
  
  // Eseguo il comando qui e salvo il valore
  ret_value = executeCommand(command, parameter);
}



byte executeCommand(byte command, byte parameter) {
  byte ret = 0;
  
  switch(command) {
    case COMMAND_BUTTON_COUNT:
      ret = BUTTON_COUNT;
      break;
    case COMMAND_BACKLIGHT:
      analogWrite(PIN_BACKLIGHT, parameter);
      break;
    case COMMAND_RESET:
      request_reset = true;
      break;
    case COMMAND_REBOUNCE:
      keypad.setDebounceTime(parameter);
    
      break;
    case COMMAND_GET_BUTTONS:
      ret = btn_state;
      
      btn_state = 0;
      
      break;
  }
  
  return ret;
}
